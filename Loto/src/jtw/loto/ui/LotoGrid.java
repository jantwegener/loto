package jtw.loto.ui;

import static java.awt.event.InputEvent.CTRL_DOWN_MASK;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Stack;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.loto.action.LotoAddNumberAction;
import jtw.loto.action.LotoCheckBoxClickedListener;
import jtw.loto.action.LotoDeleteNumberAction;
import jtw.loto.action.LotoKeyAction;
import jtw.loto.action.LotoLanguageSelectAction;
import jtw.loto.action.LotoPauseAction;
import jtw.loto.action.LotoRedoAction;
import jtw.loto.action.LotoUndoAction;
import jtw.loto.action.LotoUndoRedoAction;
import jtw.loto.settings.LotoColor;
import jtw.loto.settings.LotoSettingsDialog;
import jtw.util.IntegerUtil;

public class LotoGrid extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Log LOGGER = LogFactory.getLog(LotoGrid.class);

	private final LotoCheckBox[] checkboxArray = new LotoCheckBox[90];
	
	private final List<Integer> activeIndexList = new ArrayList<>();

	private final Stack<LotoUndoRedoAction> undoStack = new Stack<>();
	private final Stack<LotoUndoRedoAction> redoStack = new Stack<>();
	
	private int lastActivated = -1;
	
	private JFrame parentFrame;
	
	private boolean isInFullscreen = false;
	
	private JMenuBar menuBar = null;
	
	private final static String SAVE_FOLDER = "./save";
	
	private LotoColor colorManager = new LotoColor();
	
	public LotoGrid(JFrame frame) {
		this.parentFrame = frame;
		
		colorManager.loadColors();

		frame.setGlassPane(new LotoGlassPane(colorManager));
		
		setLayout(new GridLayout(9, 10));
		
		init();
		
		// call clear to set the colors correctly
		clear();
	}
	
	private void clearStack() {
		undoStack.clear();
		redoStack.clear();
	}
	
	public void activate(Integer number) {
		LotoUndoRedoAction action;
		if (activeIndexList.contains(number)) {
			action = new LotoDeleteNumberAction(activeIndexList, activeIndexList.indexOf(number));
		} else {
			action = new LotoAddNumberAction(activeIndexList, number);
		}
		undoStack.push(action);
		
		action.doAction();
		
		// after a new action, we clear the redo stack
		redoStack.clear();
		
		recomputeState();
		repaint();
	}
		
	private void recomputeState() {
		clear();
		
		for (Integer index : activeIndexList) {
			if (0 <= index && index < checkboxArray.length) {
				checkboxArray[index].setSelected(true);
				checkboxArray[index].setBackground((colorManager.getOldBackgroundColor()));
				checkboxArray[index].setFont(colorManager.getFont());
				
				lastActivated = index;
			} else {
				if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("Index out of range: " + index);
				}
			}
		}
		
		if (lastActivated >= 0) {
			checkboxArray[lastActivated].setBackground(colorManager.getNewBackgroundColor());
		}
	}
	
	private void clear() {
		// clear the states of all check boxes
		for (int i = 0; i < checkboxArray.length; i++) {
			checkboxArray[i].setSelected(false);
			checkboxArray[i].setBackground(colorManager.getOldBackgroundColor());
			checkboxArray[i].setFont(colorManager.getFont());
			checkboxArray[i].repaint();
		}
	}
	
	private void init() {
		clearStack();
		
		for (int i = 0; i < checkboxArray.length; i++) {
			LotoCheckBox cb = new LotoCheckBox(String.valueOf(i + 1), false);
			cb.setSelectedIcon(null);
			cb.addActionListener(new LotoCheckBoxClickedListener(this));
			
			checkboxArray[i] = cb;
			add(cb);
		}
		
		AbstractAction numberAction = new LotoKeyAction(this);
		
		LotoPauseAction pauseAction = new LotoPauseAction(parentFrame, colorManager);

		AbstractAction undoAction = new LotoUndoAction(this);
		AbstractAction redoAction = new LotoRedoAction(this);
		
		@SuppressWarnings("serial")
		AbstractAction resetAction = new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				// verify that the user really wants to clear everything
				int response = JOptionPane.showConfirmDialog(parentFrame, "Do you want to clear the board?", "Warning", JOptionPane.YES_NO_OPTION);
				boolean clearAll = response == JOptionPane.YES_OPTION;
				
				if (clearAll) {
					save();
					
					clear();
					
					activeIndexList.clear();
				}
			}
		};
		
		@SuppressWarnings("serial")
		AbstractAction fullscreenAction = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (isInFullscreen) {
					exitFullscreen();
				} else {
					enterFullscreen();
				}
			}
		};
		
		@SuppressWarnings("serial")
		AbstractAction quitAction = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
				
				parentFrame.setVisible(false);
				parentFrame.dispose();
				
				System.exit(0);
			}
		};

		@SuppressWarnings("serial")
		AbstractAction exitAction = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if (pauseAction.isPaused()) {
					pauseAction.exitPause();
				} else {
					exitFullscreen();
				}
			}
		};

		@SuppressWarnings("serial")
		AbstractAction saveAction = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		};
		
		@SuppressWarnings("serial")
		AbstractAction loadAction = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				load();
			}
		};

		@SuppressWarnings("serial")
		AbstractAction colorAction = new AbstractAction() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				LotoSettingsDialog dialog = new LotoSettingsDialog(colorManager);
				dialog.setModal(true);
				dialog.setSize(new Dimension(800, 400));
				dialog.setVisible(true);
				
				// save the color settings
				colorManager.saveColors();
				
				// loop through the boxes
				for (int i = 0; i < checkboxArray.length; i++) {
					checkboxArray[i].setBackground(colorManager.getOldBackgroundColor());
					checkboxArray[i].setFont(colorManager.getFont());
					checkboxArray[i].repaint();
				}

				if (lastActivated >= 0) {
					checkboxArray[lastActivated].setBackground(colorManager.getNewBackgroundColor());
				}
			}
		};
		
		InputMap map = this.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0), "resetKeyAction");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_P, 0), "pause");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_R, 0), "reset");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_R, CTRL_DOWN_MASK), "reset");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_Z, CTRL_DOWN_MASK), "undo");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_Y, CTRL_DOWN_MASK), "redo");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enter");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_F, 0), "fullscreen");
		map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "exit");
		map.put(KeyStroke.getKeyStroke('0'), "vk_0");
		map.put(KeyStroke.getKeyStroke('1'), "vk_1");
		map.put(KeyStroke.getKeyStroke('2'), "vk_2");
		map.put(KeyStroke.getKeyStroke('3'), "vk_3");
		map.put(KeyStroke.getKeyStroke('4'), "vk_4");
		map.put(KeyStroke.getKeyStroke('5'), "vk_5");
		map.put(KeyStroke.getKeyStroke('6'), "vk_6");
		map.put(KeyStroke.getKeyStroke('7'), "vk_7");
		map.put(KeyStroke.getKeyStroke('8'), "vk_8");
		map.put(KeyStroke.getKeyStroke('9'), "vk_9");
		
		ActionMap actionMap = this.getActionMap();
		actionMap.put("resetKeyAction", numberAction);
		actionMap.put("pause", pauseAction);
		actionMap.put("reset", resetAction);
		actionMap.put("enter", numberAction);
		actionMap.put("undo", undoAction);
		actionMap.put("redo", redoAction);
		actionMap.put("fullscreen", fullscreenAction);
		actionMap.put("exit", exitAction);
		actionMap.put("vk_0", numberAction);
		actionMap.put("vk_1", numberAction);
		actionMap.put("vk_2", numberAction);
		actionMap.put("vk_3", numberAction);
		actionMap.put("vk_4", numberAction);
		actionMap.put("vk_5", numberAction);
		actionMap.put("vk_6", numberAction);
		actionMap.put("vk_7", numberAction);
		actionMap.put("vk_8", numberAction);
		actionMap.put("vk_9", numberAction);
		
		menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		JMenuItem loadMenuItem = new JMenuItem(loadAction);
		loadMenuItem.setText("Load...");
		JMenuItem saveMenuItem = new JMenuItem(saveAction);
		saveMenuItem.setText("Save");
		JMenuItem resetMenuItem = new JMenuItem(resetAction);
		resetMenuItem.setText("Reset Board (R)...");
		JMenuItem pauseMenuItem = new JMenuItem(pauseAction);
		pauseMenuItem.setText("Pause (P)");
		JMenuItem exitMenuItem = new JMenuItem(quitAction);
		exitMenuItem.setText("Exit");
		
		JMenu editMenu = new JMenu("Edit");
		JMenuItem undoMenuItem = new JMenuItem(undoAction);
		undoMenuItem.setText("Undo (Ctrl+Z)");
		JMenuItem redoMenuItem = new JMenuItem(redoAction);
		redoMenuItem.setText("Redo (Ctrl+Y)");
		
		JMenu viewMenu = new JMenu("View");
		JMenuItem toggleFullscreenMenuItem = new JMenuItem(fullscreenAction);
		toggleFullscreenMenuItem.setText("Toggle Fullscreen (F)");

		JMenu settingsMenu = new JMenu("Settings");
		JMenuItem colorMenuItem = new JMenuItem(colorAction);
		colorMenuItem.setText("Loto Settings");
		JMenu languageMenu = new JMenu("Language");
		JRadioButtonMenuItem langEnglishMenuItem = new JRadioButtonMenuItem();
		JRadioButtonMenuItem langGermanMenuItem = new JRadioButtonMenuItem();
		JRadioButtonMenuItem langFrenchMenuItem = new JRadioButtonMenuItem();
	    ButtonGroup languageGroup = new ButtonGroup();
	    languageGroup.add(langEnglishMenuItem);
	    languageGroup.add(langGermanMenuItem);
	    languageGroup.add(langFrenchMenuItem);
	    
	    Map<JMenuItem, String> menuTextMap = new HashMap<>();
	    menuTextMap.put(fileMenu, "menu.file");
	    menuTextMap.put(loadMenuItem, "menu.file.load");
	    menuTextMap.put(saveMenuItem, "menu.file.save");
	    menuTextMap.put(resetMenuItem, "menu.file.reset");
	    menuTextMap.put(pauseMenuItem, "menu.file.pause");
	    menuTextMap.put(exitMenuItem, "menu.file.exit");
	    menuTextMap.put(editMenu, "menu.edit");
	    menuTextMap.put(undoMenuItem, "menu.edit.undo");
	    menuTextMap.put(redoMenuItem, "menu.edit.redo");
	    menuTextMap.put(viewMenu, "menu.view");
	    menuTextMap.put(toggleFullscreenMenuItem, "menu.view.fullscreen");
	    menuTextMap.put(settingsMenu, "menu.settings");
	    menuTextMap.put(colorMenuItem, "menu.settings.loto");
	    menuTextMap.put(languageMenu, "menu.settings.lang");
	    
	    langEnglishMenuItem.setAction(new LotoLanguageSelectAction(Locale.US, menuTextMap, "jtw.loto.i18n.menu", colorManager));
		langEnglishMenuItem.setText("English");
	    langGermanMenuItem.setAction(new LotoLanguageSelectAction(Locale.GERMANY, menuTextMap, "jtw.loto.i18n.menu", colorManager));
		langGermanMenuItem.setText("Deutsch");
	    langFrenchMenuItem.setAction(new LotoLanguageSelectAction(Locale.FRANCE, menuTextMap, "jtw.loto.i18n.menu", colorManager));
		langFrenchMenuItem.setText("Fran�ais");
		
		Locale currentLocale = colorManager.getLocale();
		if (currentLocale.getLanguage().equals("en")) {
			langEnglishMenuItem.setSelected(true);
			langEnglishMenuItem.doClick();
		} else if (currentLocale.getLanguage().equals("de")) {
			langGermanMenuItem.setSelected(true);
			langGermanMenuItem.doClick();
		} else if (currentLocale.getLanguage().equals("fr")) {
			langFrenchMenuItem.setSelected(true);
			langFrenchMenuItem.doClick();
		} else {
			langEnglishMenuItem.setSelected(true);
			langEnglishMenuItem.doClick();
		}
		
		fileMenu.add(resetMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(pauseMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(loadMenuItem);
		fileMenu.add(saveMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exitMenuItem);
		
		editMenu.add(undoMenuItem);
		editMenu.add(redoMenuItem);
		
		viewMenu.add(toggleFullscreenMenuItem);
		
		settingsMenu.add(colorMenuItem);
		settingsMenu.add(languageMenu);
		languageMenu.add(langEnglishMenuItem);
		languageMenu.add(langGermanMenuItem);
		languageMenu.add(langFrenchMenuItem);
		
		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(viewMenu);
		menuBar.add(settingsMenu);
		
		parentFrame.setJMenuBar(menuBar);
	}
	
	private void save() {
		File saveFolder = new File(SAVE_FOLDER);
		if (!saveFolder.exists()) {
			saveFolder.mkdir();
		}
		
		LocalDateTime dt = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");
		File saveFile = new File(SAVE_FOLDER + "/" + formatter.format(dt) + ".loto");
		
		try (BufferedWriter writer = new BufferedWriter(new FileWriter(saveFile))) {
			for (Integer index : activeIndexList) {
				writer.append("" + index);
				writer.newLine();
			}
		} catch (IOException ioe) {
			LOGGER.error(ioe.getMessage(), ioe);
			JOptionPane.showMessageDialog(parentFrame, "Error while saving occured.", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	private void load() {
		File saveFolder = new File(SAVE_FOLDER);

		JFileChooser chooser = new JFileChooser(saveFolder);
		FileNameExtensionFilter filter = new FileNameExtensionFilter("Loto Save Files", "loto");
		chooser.setFileFilter(filter);
		chooser.setMultiSelectionEnabled(false);
		int returnVal = chooser.showOpenDialog(parentFrame);
		
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			activeIndexList.clear();
			try (BufferedReader reader = new BufferedReader(new FileReader(chooser.getSelectedFile()))) {
				String nextLine = null;
				while ((nextLine = reader.readLine()) != null) {
					int cbIndex = IntegerUtil.toInt(nextLine, -1);
					if (LOGGER.isDebugEnabled()) {
						LOGGER.debug("nextLine: " + nextLine + "; cbIndex: " + cbIndex);
					}
					if (cbIndex >= 0) {
						activeIndexList.add(cbIndex);
					}
				}
			} catch (IndexOutOfBoundsException | IOException ioe) {
				LOGGER.error(ioe.getMessage(), ioe);
				JOptionPane.showMessageDialog(parentFrame, "Error while saving occured.", "Error", JOptionPane.ERROR_MESSAGE);
			}
			// clear the current stacks
			clearStack();
			// finally recompute the current state
			recomputeState();
		}
	}
	
	private void enterFullscreen() {
		GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
		parentFrame.setVisible(false);
		parentFrame.dispose();
		parentFrame.setUndecorated(true);
		parentFrame.setJMenuBar(null);
		device.setFullScreenWindow(parentFrame);
		parentFrame.setVisible(true);
		
		isInFullscreen = true;
	}
	
	private void exitFullscreen() {
		if (isInFullscreen) {
			GraphicsDevice device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[0];
			parentFrame.setVisible(false);
			parentFrame.dispose();
			parentFrame.setUndecorated(false);
			parentFrame.setJMenuBar(menuBar);
			device.setFullScreenWindow(null);
			parentFrame.setVisible(true);
	
			isInFullscreen = false;
		}
	}

	public void undo() {
		if (!undoStack.isEmpty()) {
			LotoUndoRedoAction action = undoStack.pop();
			action.undoAction();
			redoStack.push(action);
		}
		
		recomputeState();
		repaint();
	}
	
	public void redo() {
		if (!redoStack.isEmpty()) {
			LotoUndoRedoAction action = redoStack.pop();
			action.redoAction();
			undoStack.push(action);
		}
		
		recomputeState();
		repaint();
	}
	
}
