package jtw.loto.ui;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JCheckBox;

public class LotoCheckBox extends JCheckBox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public LotoCheckBox(String text, boolean state) {
		super(text, state);
	}
	
    public void paint(Graphics g) {
    	// background
    	if (isSelected()) {
    		g.setColor(getBackground());
    	} else {
    		g.setColor(Color.WHITE);
    	}
    	g.fillRect(0, 0, getWidth(), getHeight());

    	g.setColor(Color.BLACK);
    	g.drawRect(0, 0, getWidth(), getHeight());

    	g.setFont(getFont());
    	
    	// text
    	double width = g.getFontMetrics().stringWidth(getText());
    	double height = g.getFontMetrics().getAscent();
    	double centerX = (getWidth() - width) / 2d;
    	double centerY = (getHeight() + height) / 2d - g.getFontMetrics().getLineMetrics(getText(), g).getUnderlineOffset();
    	
    	g.drawString(getText(), (int)centerX, (int)centerY);
    }

}
