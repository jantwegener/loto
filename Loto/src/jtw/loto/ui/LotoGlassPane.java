package jtw.loto.ui;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.JComponent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.loto.settings.LotoColor;

public class LotoGlassPane extends JComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Log LOGGER = LogFactory.getLog(LotoGlassPane.class);

	private List<File> imageFileList = List.of();

	private int delay;

	private int currentImageIndex = 0;

	private Image currentImage = null;

	private long lastChange;

	private LotoColor colorManager;

	private Thread imageChangeThread;

	public LotoGlassPane(LotoColor colorManager) {
		this.colorManager = colorManager;

		// deactivate the mouse
		addMouseListener(new MouseAdapter() {});
	}

	public void init() {
		delay = colorManager.getPauseImageDelay() * 1000;

		File imageFolder = colorManager.getPauseImageFolder();
		if (imageFolder.isDirectory()) {
			imageFileList = List.of(imageFolder.listFiles(new FileFilter() {
				@Override
				public boolean accept(File pathname) {
					for (String extension : List.of("jpg", "gif", "png")) {
						if (pathname.getName().endsWith(extension)) {
							return true;
						}
					}
					return false;
				}}));

			if (!imageFileList.isEmpty()) {
				try {
					currentImage = ImageIO.read(imageFileList.get(0));
				} catch (IOException e) {
					LOGGER.error("Exception while reading image file: " + imageFileList.get(0), e);
				}
			}
		}
	}

	@Override
	public void setVisible(boolean visible) {
		if (!imageFileList.isEmpty()) {
			if (visible) {
				lastChange = System.currentTimeMillis();
				final LotoGlassPane thisPane = this;
				imageChangeThread = new Thread(new Runnable() {

					@Override
					public void run() {
						while (true) {
							thisPane.repaint();
							try {
								Thread.sleep(500);
							} catch (InterruptedException e) {
								LOGGER.error("Interrupted while sleeping", e);
							}
						}

					}
				});
				imageChangeThread.start();
			} else {
				if (imageChangeThread != null) {
					imageChangeThread.interrupt();
				}
			}
		}
		super.setVisible(visible);
	}

	@Override
	protected void paintComponent(Graphics g) {
		if (imageFileList.isEmpty()) {
			g.setColor(new Color(60, 60, 60, 160));
			g.fillRect(0, 0, getWidth(), getHeight());

			g.setColor(Color.WHITE);
			Font f = new Font(Font.SERIF, Font.BOLD, 80);
			g.setFont(f);
			int x = (getWidth() - g.getFontMetrics().stringWidth("PAUSE")) / 2;
			int y = getHeight() / 2;
			g.drawString("PAUSE", x, y);
		} else {
			long now = System.currentTimeMillis();
			if (now - lastChange > delay) {
				currentImageIndex++;
				if (currentImageIndex >= imageFileList.size()) {
					currentImageIndex = 0;
				}

				try {
					currentImage = ImageIO.read(imageFileList.get(currentImageIndex));
				} catch (IOException e) {
					LOGGER.error("Exception while reading image file: " + imageFileList.get(currentImageIndex), e);
				}
				lastChange = System.currentTimeMillis();
			}
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, getWidth(), getHeight());
			int imgWidth = currentImage.getWidth(null);
			int imgHeight = currentImage.getHeight(null);
			double scaleX = (double) getWidth() / imgWidth;
			double scaleY = (double) getHeight() / imgHeight;
			double scale;
			if (scaleX < scaleY) {
				scale = scaleX;
			} else {
				scale = scaleY;
			}
			imgWidth = (int) (imgWidth * scale);
			imgHeight = (int) (imgHeight * scale);
			g.drawImage(currentImage, (getWidth() - imgWidth) / 2, (getHeight() - imgHeight) / 2, imgWidth, imgHeight, null);
		}
	}

}
