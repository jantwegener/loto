package jtw.loto.settings;

import java.awt.Color;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;

import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.util.IntegerUtil;

public class LotoColor {

	private static Log LOGGER = LogFactory.getLog(LotoColor.class);
	
	private static Set<String> SUPPORTED_LANGUAGES_SET = Set.of("en", "de", "fr");
	
	private Color oldBackgroundColor = Color.YELLOW;
	
	private Color newBackgroundColor = Color.GREEN;
	
	private Font lotoFont = new Font("Arial", Font.BOLD, 22); 
	
	private File pauseImageFolder;
	
	private int pauseImageDelay;
	
	private Locale locale;
	
	private final static File PREF_FILE = new File("./pref/loto_pref.txt");
	
	public LotoColor() {
	}
	
	public void loadColors() {
		// init the colors
		Properties p = new Properties();
		try (BufferedReader reader = new BufferedReader(new FileReader(PREF_FILE))) {
			p.load(reader);
		} catch (IOException e) {
			LOGGER.error("Error while loading from preference file: " + PREF_FILE.getAbsolutePath());
			LOGGER.error(e.getMessage(), e);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			JOptionPane.showMessageDialog(null, "Error while initializing the settings.", "Error", JOptionPane.ERROR_MESSAGE);
		}

		// colors
		String oldString = p.getProperty("old_color", "255 255 0");
		String newString = p.getProperty("new_color", "0 255 0");

		String[] oldSplit = oldString.split("\\s+");
		String[] newSplit = newString.split("\\s+");

		int or = IntegerUtil.toInt(oldSplit[0], 255);
		int og = IntegerUtil.toInt(oldSplit[1], 255);
		int ob = IntegerUtil.toInt(oldSplit[2], 0);

		int nr = IntegerUtil.toInt(newSplit[0], 0);
		int ng = IntegerUtil.toInt(newSplit[1], 255);
		int nb = IntegerUtil.toInt(newSplit[2], 0);

		setOldBackgroundColor(new Color(or, og, ob));
		setNewBackgroundColor(new Color(nr, ng, nb));
		
		// fonts
		String fontFamily = p.getProperty("font_family", "SansSerif");
		int size = IntegerUtil.toInt(p.getProperty("font_size", "24"), 24);
		lotoFont = new Font(fontFamily, Font.BOLD, size);
		
		// pause settings
		setPauseImageFolder(new File(p.getProperty("pause_image_folder", "")));
		setPauseImageDelay(IntegerUtil.toInt(p.getProperty("pause_image_delay", "0"), 0));
		
		// locale
		String localeLang = p.getProperty("locale_lang");
		if (localeLang == null) {
			localeLang = Locale.getDefault().getLanguage();
		}
		if (!SUPPORTED_LANGUAGES_SET.contains(localeLang)) {
			localeLang = "en";
		}
		// backwards compatibility to Java 17
		locale = new Locale(localeLang);
	}
	
	public void saveColors() {
		if (PREF_FILE.getParentFile().exists() || PREF_FILE.getParentFile().mkdirs()) {
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(PREF_FILE))) {
				Properties p = new Properties();
				
				String oldString = oldBackgroundColor.getRed() + " " + oldBackgroundColor.getGreen() + " " + oldBackgroundColor.getBlue();
				String newString = newBackgroundColor.getRed() + " " + newBackgroundColor.getGreen() + " " + newBackgroundColor.getBlue();
						
				p.setProperty("old_color", oldString);
				p.setProperty("new_color", newString);
				
				p.setProperty("font_family", lotoFont.getFamily());
				p.setProperty("font_size", Integer.toString(lotoFont.getSize()));
				
				p.setProperty("pause_image_folder", pauseImageFolder.getAbsolutePath());
				p.setProperty("pause_image_delay", Integer.toString(pauseImageDelay));
				
				p.setProperty("locale_lang", locale.getLanguage());
				
				p.store(writer, null);
			} catch (IOException e) {
				LOGGER.error("Error while saving to preference file: " + PREF_FILE.getAbsolutePath());
				LOGGER.error(e.getMessage(), e);
				JOptionPane.showMessageDialog(null, "Error while initializing the colors.", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	public Color getOldBackgroundColor() {
		return oldBackgroundColor;
	}

	public void setOldBackgroundColor(Color oldBackgroundColor) {
		this.oldBackgroundColor = oldBackgroundColor;
	}

	public Color getNewBackgroundColor() {
		return newBackgroundColor;
	}

	public void setNewBackgroundColor(Color newBackgroundColor) {
		this.newBackgroundColor = newBackgroundColor;
	}
	
	public void setFont(Font font) {
		this.lotoFont = font;
	}
	
	public Font getFont() {
		return lotoFont;
	}

	public File getPauseImageFolder() {
		return pauseImageFolder;
	}

	public void setPauseImageFolder(File pauseImageFolder) {
		this.pauseImageFolder = pauseImageFolder;
	}

	public int getPauseImageDelay() {
		return pauseImageDelay;
	}

	public void setPauseImageDelay(int pauseImageDelay) {
		this.pauseImageDelay = pauseImageDelay;
	}

	public Locale getLocale() {
		return locale;
	}
	
	public void setLocale(Locale locale) {
		this.locale = locale;
	}

}
