package jtw.loto.settings;

import static java.awt.Font.DIALOG;
import static java.awt.Font.DIALOG_INPUT;
import static java.awt.Font.MONOSPACED;
import static java.awt.Font.SANS_SERIF;
import static java.awt.Font.SERIF;
import static javax.swing.JFileChooser.APPROVE_OPTION;
import static javax.swing.JFileChooser.DIRECTORIES_ONLY;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import jtw.util.IntegerUtil;

public class LotoSettingsDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LotoColor colorManager;

	public LotoSettingsDialog(LotoColor colorManager) {
		this.colorManager = colorManager;

		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		init();
	}

	private void init() {
		JPanel pColor = new JPanel(new GridLayout(1, 2));
		final JButton oldBackground = new JButton("Old Cell");
		final JButton newBackground = new JButton("New Cell");

		oldBackground.setBackground(colorManager.getOldBackgroundColor());
		newBackground.setBackground(colorManager.getNewBackgroundColor());

		oldBackground.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JColorChooser chooser = new JColorChooser(colorManager.getOldBackgroundColor());

				ActionListener okListener = new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Color selectedColor = chooser.getColor();
						colorManager.setOldBackgroundColor(selectedColor);
						oldBackground.setBackground(colorManager.getOldBackgroundColor());
					}
				};

				JDialog colorDialog = JColorChooser.createDialog(pColor, "Title", true, chooser, okListener, null);
				colorDialog.setVisible(true);
			}
		});

		newBackground.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JColorChooser chooser = new JColorChooser(colorManager.getNewBackgroundColor());

				ActionListener okListener = new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						Color selectedColor = chooser.getColor();
						colorManager.setNewBackgroundColor(selectedColor);
						newBackground.setBackground(colorManager.getNewBackgroundColor());
					}
				};

				JDialog colorDialog = JColorChooser.createDialog(pColor, "Title", true, chooser, okListener, null);
				colorDialog.setVisible(true);
			}
		});

		pColor.add(oldBackground);
		pColor.add(newBackground);

		JPanel southPanel = new JPanel(new BorderLayout());

		JPanel pFont = new JPanel(new BorderLayout());
		JLabel label = new JLabel("01 12...78 89");
		label.setFont(colorManager.getFont());

		JComboBox<String> fontFamilyTextfield = new JComboBox<>(new String[] { DIALOG, DIALOG_INPUT, MONOSPACED, SERIF, SANS_SERIF });
		fontFamilyTextfield.setEditable(false);
		fontFamilyTextfield.setSelectedItem(colorManager.getFont().getFamily());

		JComboBox<Integer> fontSizeComboBox = new JComboBox<>(new Integer[] {12, 16, 20, 24, 30, 36, 42, 48, 54, 60, 65, 70, 75, 80, 90});
		fontSizeComboBox.setEditable(true);
		fontSizeComboBox.setSelectedItem(colorManager.getFont().getSize());

		FontLabelListener listener = new FontLabelListener(label, fontFamilyTextfield, fontSizeComboBox, colorManager.getFont());
		fontFamilyTextfield.addActionListener(listener);
		fontSizeComboBox.addActionListener(listener);

		JPanel ptFont = new JPanel(new GridLayout(2, 1));
		ptFont.add(fontFamilyTextfield);
		ptFont.add(fontSizeComboBox);

		pFont.add(label, BorderLayout.CENTER);
		pFont.add(ptFont, BorderLayout.EAST);

		JPanel pausePanel = new JPanel();
		JTextField pauseImageFolderTextField = new JTextField(colorManager.getPauseImageFolder().getAbsolutePath(), 30);
		pauseImageFolderTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				textChanged();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				textChanged();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				textChanged();
			}

			private void textChanged() {
				File imageFolder = new File(pauseImageFolderTextField.getText());
				colorManager.setPauseImageFolder(imageFolder);
			}
		});
		JButton pauseImageFolderButton = new JButton("Choose pause image folder");
		JDialog thisDialog = this;
		pauseImageFolderButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser(colorManager.getPauseImageFolder());
				fileChooser.setFileSelectionMode(DIRECTORIES_ONLY);
				if (fileChooser.showOpenDialog(thisDialog) == APPROVE_OPTION) {
					File imageFolder = fileChooser.getSelectedFile();
					pauseImageFolderTextField.setText(imageFolder.getAbsolutePath());
				}
			}
		});
		JTextField pauseImageDelayTextField = new JTextField("" + colorManager.getPauseImageDelay(), 8);
		pauseImageDelayTextField.getDocument().addDocumentListener(new DocumentListener() {

			@Override
			public void removeUpdate(DocumentEvent e) {
				textChanged();
			}

			@Override
			public void insertUpdate(DocumentEvent e) {
				textChanged();
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				textChanged();
			}

			private void textChanged() {
				int delay = IntegerUtil.toInt(pauseImageDelayTextField.getText(), 0);
				colorManager.setPauseImageDelay(delay);
			}
		});
		pausePanel.add(pauseImageFolderTextField);
		pausePanel.add(pauseImageFolderButton);
		pausePanel.add(new JLabel("    "));
		pausePanel.add(new JLabel("Delay (in s): "));
		pausePanel.add(pauseImageDelayTextField);

		southPanel.add(pFont, BorderLayout.CENTER);
		southPanel.add(pausePanel, BorderLayout.SOUTH);

		JPanel main = new JPanel(new BorderLayout());
		main.add(pColor, BorderLayout.CENTER);
		main.add(southPanel, BorderLayout.SOUTH);

		add(main);
	}

	private class FontLabelListener implements ActionListener {

		private Font font;

		private final JLabel label;
		private final JComboBox<String> fontFamilyComboBox;
		private final JComboBox<Integer> fontSizeComboBox;

		public FontLabelListener(JLabel label, JComboBox<String> fontFamilyTextfield, JComboBox<Integer> fontSizeComboBox, Font font) {
			this.label = label;
			this.fontFamilyComboBox = fontFamilyTextfield;
			this.fontSizeComboBox = fontSizeComboBox;
			this.font = font;
		}

		private void updateFont() {
			font = new Font((String) fontFamilyComboBox.getSelectedItem(), Font.BOLD, (Integer) fontSizeComboBox.getSelectedItem());
			label.setFont(font);
			colorManager.setFont(font);
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			updateFont();
		}

	}


}
