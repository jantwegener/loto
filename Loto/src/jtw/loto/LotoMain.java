package jtw.loto;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;

import jtw.loto.ui.LotoGrid;

public class LotoMain {
	
	private static final String VERSION = "1.2";
	
	public static void main(String[] arg) {
		JFrame frame = new JFrame();
		frame.setTitle("Loto (v" + VERSION + ")");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().add(getMainPanel(frame));
		
		frame.setSize(800, 600);
		
		frame.setVisible(true);
	}
	
	public static JPanel getMainPanel(JFrame frame) {
		JPanel p = new JPanel(new BorderLayout());
		
		LotoGrid lg = new LotoGrid(frame);
		
		p.add(lg);
		
		return p;
	}
	
}
