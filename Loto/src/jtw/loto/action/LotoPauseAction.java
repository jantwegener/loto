package jtw.loto.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JFrame;

import jtw.loto.settings.LotoColor;
import jtw.loto.ui.LotoGlassPane;

public class LotoPauseAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private LotoGlassPane glassPane;
	
	private boolean isPaused = false;
	
	public LotoPauseAction(JFrame frame, LotoColor colorManager) {
		glassPane = new LotoGlassPane(colorManager);
		frame.setGlassPane(glassPane);
	}
	
	public void exitPause() {
		isPaused = false;
		glassPane.setVisible(isPaused);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		glassPane.init();
		isPaused = !isPaused;
		glassPane.setVisible(isPaused);
	}

	public boolean isPaused() {
		return isPaused;
	}

}
