package jtw.loto.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import jtw.loto.ui.LotoCheckBox;
import jtw.loto.ui.LotoGrid;

public class LotoCheckBoxClickedListener implements ActionListener {
	
	private LotoGrid grid;

	public LotoCheckBoxClickedListener(LotoGrid grid) {
		this.grid = grid;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() instanceof LotoCheckBox) {
			LotoCheckBox box = (LotoCheckBox) e.getSource();
			String boxText = box.getText();
			grid.activate(Integer.valueOf(boxText) - 1);
		}
	}

}
