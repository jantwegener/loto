package jtw.loto.action;

public interface LotoUndoRedoAction {
	
	public void doAction();
	
	public void undoAction();
	
	public void redoAction();
	
}
