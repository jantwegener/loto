package jtw.loto.action;

import java.util.List;

public class LotoAddNumberAction implements LotoUndoRedoAction {

	private final List<Integer> activeNumberList;
	private final Integer numberToAdd;
	
	private int index = -1;
	
	public LotoAddNumberAction(List<Integer> activeNumberList, Integer numberToAdd) {
		this.activeNumberList = activeNumberList;
		this.numberToAdd = numberToAdd;
	}

	@Override
	public void doAction() {
		index = activeNumberList.size();
		activeNumberList.add(numberToAdd);
	}

	@Override
	public void undoAction() {
		if (index >= 0) {
			activeNumberList.remove(index);
			index = -1;
		}
	}

	@Override
	public void redoAction() {
		if (index < 0) {
			doAction();
		}
	}

}
