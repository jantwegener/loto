package jtw.loto.action;

import java.awt.event.ActionEvent;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import javax.swing.AbstractAction;
import javax.swing.JMenuItem;

import jtw.loto.settings.LotoColor;

public class LotoLanguageSelectAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final Locale locale;
	private final Map<JMenuItem, String> menuTextMap;
	private final String langResourceBundleName;
	private final LotoColor colorManager;
	
	public LotoLanguageSelectAction(Locale locale, Map<JMenuItem, String> menuTextMap, String resourceBundleName, LotoColor colorManager) {
		this.locale = locale;
		this.menuTextMap = menuTextMap;
		this.langResourceBundleName = resourceBundleName;
		this.colorManager = colorManager;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ResourceBundle langResourceBundle = ResourceBundle.getBundle(langResourceBundleName, locale);
		for (Map.Entry<JMenuItem, String> entry : menuTextMap.entrySet()) {
			JMenuItem comp = entry.getKey();
			String key = entry.getValue();
			String text = langResourceBundle.getString(key);
			comp.setText(text);
		}
		colorManager.setLocale(locale);
		colorManager.saveColors();
	}

}
