package jtw.loto.action;

import java.util.List;

public class LotoDeleteNumberAction implements LotoUndoRedoAction {
	
	private final List<Integer> activeNumberList;
	private final int indexToDelete;
	
	private Integer removedNumber;
	
	public LotoDeleteNumberAction(List<Integer> activeNumberList, int indexToDelete) {
		this.activeNumberList = activeNumberList;
		this.indexToDelete = indexToDelete;
	}

	@Override
	public void doAction() {
		removedNumber = activeNumberList.remove(indexToDelete);
	}

	@Override
	public void undoAction() {
		if (removedNumber != null) {
			activeNumberList.add(indexToDelete, removedNumber);
			removedNumber = null;
		}
	}

	@Override
	public void redoAction() {
		if (removedNumber == null) {
			doAction();
		}
	}

}
