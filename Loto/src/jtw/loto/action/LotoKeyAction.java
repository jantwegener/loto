package jtw.loto.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jtw.loto.ui.LotoGrid;

public class LotoKeyAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Log LOGGER = LogFactory.getLog(LotoKeyAction.class);
	
	private LotoGrid grid;
	
	private String number = "";
	
	public LotoKeyAction(LotoGrid grid) {
		this.grid = grid;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if ("\b".equals(e.getActionCommand())) {
			number = "";
		} else if ("\n".equals(e.getActionCommand())) {
			activateNumber();
		} else {
			number += e.getActionCommand();
			if (number.length() >= 2) {
				activateNumber();
			}
		}
	}
	
	private void activateNumber() {
		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("entering: " + number);
		}
		
		try {
			int num = Integer.parseInt(number);
			grid.activate(num - 1);
			number = "";
		} catch (NumberFormatException e) {
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Number not recognized: " + number);
			}
		}
	}

}
