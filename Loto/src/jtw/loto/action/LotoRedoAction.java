package jtw.loto.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import jtw.loto.ui.LotoGrid;

public class LotoRedoAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private LotoGrid grid;

	public LotoRedoAction(LotoGrid grid) {
		this.grid = grid;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		grid.redo();	
	}

}
